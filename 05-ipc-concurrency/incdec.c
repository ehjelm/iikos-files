#include <pthread.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/syscall.h>
#include <sys/types.h>
#include <unistd.h>

#define NITER 10

int g_ant=0;

void *IncThread() {
    int i;
    for (i = 0; i < NITER; i++) {
    	g_ant++;
    }
    return 0;
}

void *DecThread() {
    int i;
    for (i = 0; i < NITER; i++) {
    	g_ant--;
    }
    return 0;
}

int main(void) {
    pthread_t tid1, tid2;

    pthread_create(&tid1, NULL, IncThread, NULL); /* start tråder */
    pthread_create(&tid2, NULL, DecThread, NULL);
    pthread_join(tid1, NULL);                     /* vent på tråder */
    pthread_join(tid2, NULL); 

    if (g_ant != 0) {
        printf(" HUFF! g_ant er %d, skulle vært 0\n", g_ant);
    } else {
        printf(" OK! g_ant er %d\n", g_ant);
    }
    return 0;
}








